MedCalc News Server
===================

Stores MedCalc news into a SQLite database, dumping all current news into a static JSON file after every change.
Used by [medcalc-news][] from within MedCalc to display news.


### Setup

You must copy `settings.inc.default` to `settings.inc`, changing the defaults where desired.
Also copy `setup.inc.default` to `setup.inc`, adjusting the default users to create.
Then call `setup.php` from your browser at least once to create the SQLite databases.


### Tech

This is a _PHP_ website, requiring PHP v5.4+, storing data into a _SQLite_ database (via PDO).


medcalc-news: https://github.com/p2/medcalc-news
