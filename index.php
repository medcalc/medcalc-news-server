<?php

require_once('init.inc');

if (DEBUG) {
	error_reporting(E_ALL);
	ini_set('display_errors', '1');
}


# check access token
session_start();
$ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null;
$db = new NewsDatabase();
$t_name = 'login.html';
$t_hash = null;


# retrieve token
$tok = null;
if (isset($_GET['t'])) {
	$tok = new Token($db, $_GET['t']);
}
else if (isset($_SESSION['token'])) {
	$tok = new Token($db, $_SESSION['token']);
}

$ok = $tok && $tok->isValid($ip);
if ($ok) {
	$_SESSION['token'] = $tok->token();
}


# show "login" form
if (!$ok) {
	$t_hash = array('title' => 'Login');
	
	if (isset($_POST['email'])) {
		$user = new User($db, $_POST['email']);
		if ($user->exists()) {
			if ($user->sendToken($ip)) {
				$t_hash['success_msg'] = "Token sent";
			}
			else {
				$t_hash['error_msg'] = "Error sending token";
			}
		}
		else {
			$t_hash['error_msg'] = "You know nothing";
		}
	}
	else if (isset($_GET['t'])) {
		$t_hash['error_msg'] = "Nice try";
	}
}


# logout
else if (isset($_POST['logout'])) {
	$t_hash = array('title' => 'Logout');
	if ($tok->delete()) {
		$t_hash['success_msg'] = "Logged out";
	}
}


# access granted
else {
	$t_name = 'news.html';
	$t_hash['token'] = $tok->token();
	
	# edit news
	if (isset($_POST['edit'])) {
		$news = new NewsItem($db, $_POST['edit']);
		$news->updateFrom($_POST);
		$news->update($tok->user());
	}
	
	# delete news
	else if (isset($_POST['delete'])) {
		$news = new NewsItem($db, $_POST['delete']);
		$news->delete($tok->user());
	}
	
	# new news
	else if (isset($_POST['text'])) {
		$news = new NewsItem($db, gen_uuid());
		$news->updateFrom($_POST);
		$news->insert($tok->user());
	}
	
	# list all news
	$platform = isset($_GET['p']) ? $_GET['p'] : (isset($_POST['p']) ? $_POST['p'] : null);
	$t_hash['platform'] = $platform;
	$t_hash['news'] = NewsItem::find($db, $platform);
}

$tpl = new Template($t_name);
$tpl->render($t_hash);


?>
