<?php

require_once('settings.inc');

spl_autoload_register(function($klass) {
	if (!class_exists($klass, false)) {
		@include('php/'.$klass.'.class.php');
	}
});

if (!ini_get('date.timezone')) {
	date_default_timezone_set('Europe/Zurich');
}

?>
