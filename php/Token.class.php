<?php

require_once(dirname(__FILE__).'/DBObject.class.php');


/**
 *  Representing an access token bound to a user by email and IP.
 */
class Token extends DBObject
{
	protected $token;
	protected $user;
	
	function __construct($db, $token) {
		parent::__construct($db);
		$this->token = $token;
	}
	
	public function token() {
		return $this->token;
	}
	
	public function user() {
		return $this->user;
	}
	
	
	/**
	 *  Returns the existing token for the given user, if one exists.
	 */
	static public function existingToken(User $user) {
		if (!$user || !$user->db()) {
			throw new Exception('Database of user is not set up');
		}
		
		$stmt = $user->db()->prepare("SELECT * FROM tokens WHERE email = ? AND strftime('%s','now') - strftime('%s', generated) < 7200");
		$stmt->execute(array($user->email()));
		$rslt = $stmt->fetch();
		
		if (!$rslt) {
			return null;
		}
		
		$tok = new self($user->db(), $rslt['token']);
		$tok->user = $user;
		
		return $tok;
	}
	
	/**
	 *  Create and store a new token for the given user, tied to the specific IP address.
	 */
	static public function newToken(User $user, $request_ip) {
		if (!$user || !$user->db()) {
			throw new Exception('Database of user is not set up');
		}
		if (!$request_ip) {
			throw new Exception('You must provide an IP address for a new token');
		}
		
		$tok = new self($user->db(), gen_uuid());
		$tok->user = $user;
		
		$stmt = $tok->db->prepare("INSERT INTO tokens (token, email, ip) VALUES (?, ?, ?)");
		if ($stmt->execute(array($tok->token, $user->email(), $request_ip)) > 0) {
			return $tok;
		}
		return null;
	}
	
	/**
	 *  A token is valid if it's less than 2 hours old and the request IP is identical.
	 */
	public function isValid($request_ip) {
		if (!$this->db) {
			throw new Exception('Database not set up');
		}
		$stmt = $this->db->prepare("SELECT email, ip FROM tokens WHERE token = ? AND strftime('%s','now') - strftime('%s', generated) < 7200");
		$stmt->execute(array($this->token));
		$rslt = $stmt->fetch();
		
		if (!$rslt) {
			return false;
		}
		
		# get the corresponding user
		if (!$this->user) {
			$this->user = new User($this->db, $rslt['email']);
		}
		$this->user->seen();
		
		# token is there and less than 2 hours old, but not yet claimed
		if (is_null($rslt['ip'])) {
			$upd = $this->db->prepare("UPDATE tokens SET ip = ? WHERE token = ?");
			$upd->execute(array($request_ip, $this->token));
			
			return true;
		}
		
		# already claimed, IP must be correct
		return $request_ip === $rslt['ip'];
	}
	
	public function _myself() {
		return array('tokens', 'token', $this->token);
	}
}


?>
