<?php

require_once('PPSQLite.class.php');


/**
 *  Easy access to our databases.
 */
class NewsDatabase extends PPSQLite
{
	function __construct() {
		parent::__construct('news.db');
	}
	
	
	public static function setup($default_users=null) {
		$instance = new self();
		$instance->exec('CREATE TABLE IF NOT EXISTS news (uid varchar primary key, `text` text, platforms varchar, geo varchar, added datetime, added_by varchar, lastedit datetime, lastedit_by varchar, deleted datetime, deleted_by varchar)');
		$instance->exec('CREATE TABLE IF NOT EXISTS users (email varchar unique, name varchar, added datetime DEFAULT CURRENT_TIMESTAMP, lastseen datetime)');
		$instance->exec('CREATE TABLE IF NOT EXISTS tokens (token varchar unique, email varchar, ip varchar, generated datetime DEFAULT CURRENT_TIMESTAMP)');
		
		# default users
		if (!is_null($default_users)) {
			$stmt = $instance->prepare('INSERT OR REPLACE INTO users (email, name) VALUES (?, ?)');
			foreach ($default_users as $email => $name) {
				$stmt->execute(array($email, $name));
			}
		}
	}
}
