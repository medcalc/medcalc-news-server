<?php


/**
 *  Wrapper for SQLite, relies on PDO to be present.
 *
 *  Looks at `DATABASE_DIR` at construction time and creates the database at that given directory, relative to the
 *  class file's parent directory, or in "database" if the constant is not set.
 */
class PPSQLite
{
	protected $dbh = null;
	
	function __construct($database) {
		if (!$database) {
			throw new Exception("Need a database name", 1);
		}
		if (!class_exists('PDO')) {
			throw new Exception("PDO is not available", 1);
		}
		
		$dirpath = defined('DATABASE_DIR') ? DATABASE_DIR : realpath(dirname(__FILE__).'/../database');
		if (!is_dir($dirpath)) {
			throw new Exception("The directory holding the database does not exist, please create it at:\n$dirpath");
		}
		$filename = $dirpath.'/'.str_replace('/', '', $database);
		if (!is_writeable(dirname($filename))) {
			throw new Exception("The directory holding the database is not writeable, please fix this at:\n$dirpath");
		}
		
		$this->dbh = new PDO('sqlite:'.$filename);
		$this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	
	public function exec($sql) {
		return $this->dbh->exec($sql);
	}
	
	public function prepare($sql) {
		return $this->dbh->prepare($sql);
	}
	
	public static function setup() {
	}
}


?>
