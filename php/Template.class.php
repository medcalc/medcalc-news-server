<?php
#
#	Template class for easy access to Twig, formerly Haanga
#
#	2011-12-04	created for Haanga
#	2014-04-03	converted for Twig
#

require_once(dirname(__FILE__).'/../settings.inc');

if (!defined('TEMPLATE_DIR')) {
	trigger_error('Template directory "TEMPLATE_DIR" has not been defined', E_USER_ERROR);
}
if (!defined('TEMPLATE_CACHE_DIR')) {
	define('TEMPLATE_CACHE_DIR', null);
}
if (null != TEMPLATE_CACHE_DIR && !is_writable(TEMPLATE_CACHE_DIR)) {
	trigger_error('Template cache directory "'.TEMPLATE_CACHE_DIR.'" is not writeable', E_USER_ERROR);
}

# include Twig
require_once(dirname(__FILE__).'/../Twig/lib/Twig/Autoloader.php');
Twig_Autoloader::register();


class Template
{
	private $filename;
	
	# constructor
	public function __construct($filename) {
		$this->filename = $filename;
	}
	
	
	public function render($hash=null) {
		print($this->apply($hash));
	}
	
	/**
	 *	Applies a hash of values to a template.
	 *	@param $hash An associative array of placeholders to be replaced
	 *	@return the template with replaced placeholders
	 */
	public function apply($hash=null) {
		if (!is_file(TEMPLATE_DIR.'/'.$this->filename)) {
			trigger_error('The template named "'.$this->filename.'" does not exist in "'.TEMPLATE_DIR.'"', E_USER_WARNING);
			return '';
		}
		
		# prepare hash
		if (!is_array($hash)) {
			$hash = array();
		}
		
		if (!isset($hash['SELF'])) {
			$hash['SELF'] = preg_replace('/referer=[^&]+/', '', isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '');
		}
		if (!isset($hash['SCHEME'])) {
			$hash['SCHEME'] = (isset($_SERVER['HTTPS']) && 'on' == $_SERVER['HTTPS']) ? 'https' : 'http';
		}
		if (!isset($hash['DOMAIN'])) {
			$hash['DOMAIN'] = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '127.0.0.1';
		}
		if (!isset($hash['ROOT'])) {
			$hash['ROOT'] = str_replace('/php', '', str_replace($_SERVER['DOCUMENT_ROOT'], '', dirname(__FILE__)));
		}
		if (!isset($hash['BASE_URL'])) {
			$hash['BASE_URL'] = $hash['SCHEME'].'://'.$hash['DOMAIN'].$hash['ROOT'];
		}
		if (!isset($hash['REFERER'])) {
			#$ref = stripCommonURLParts($_SERVER['HTTP_REFERER'], $_SERVER['SCRIPT_NAME']);
			$ref = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
			if (strlen($ref) > 0 && '?' != $ref) {
				$hash['REFERER'] = $ref;
			}
			else {
				$hash['REFERER'] = '';
			}
		}
		if (!isset($hash['COPYRIGHT_YEAR'])) {
			$hash['COPYRIGHT_YEAR'] = date('Y');
		}
		
		# configure Twig
		$config = array();
		if (TEMPLATE_CACHE_DIR) {
			$config['cache'] = TEMPLATE_CACHE_DIR;
		}
		$loader = new Twig_Loader_Filesystem(TEMPLATE_DIR);
		$twig = new Twig_Environment($loader, $config);
		
		return $twig->render($this->filename, $hash);
	}
}


?>
