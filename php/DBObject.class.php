<?php


/**
 *  A very lightweight parent class for DB objects.
 */
class DBObject
{
	protected $db = null;
	protected $exists = false;
	
	function __construct($db) {
		$this->db = $db;
	}
	
	
	public function db() {
		return $this->db;
	}
	
	
	/**
	 *  Should be called before instance-level db actions and should throw exceptions if preconditions fail.
	 */
	public function preflight($is_insert=false) {
		if (!$this->db) {
			throw new Exception("Database is not set up");
		}
	}
	
	/** Override in subclasses and return [table-name, table-key-column, key] for the instance. */
	protected function _myself() {
		return null;
	}
	
	/**
	 *  Hydrates the receiver, unless "exists" is already true.
	 */
	public function hydrate($force=false) {
		if (!$force && $this->exists) {
			return;
		}
		
		$this->preflight(false);
		
		list($hydra, $key) = $this->_hydration();
		if (!$hydra) {
			throw new Exception('Failed to hydrate "'.get_class($this).'" instance');
		}
		
		$stmt = $this->db->prepare($hydra);
		if ($stmt->execute(array($key))) {
			$rslt = $stmt->fetch();
			if ($rslt) {
				$this->exists = true;
				$this->hydrateFrom($rslt);
			}
		}
	}
	
	/** Override in subclasses and return the select query and the primary key for the instance, with one placeholder in the query that takes the primary key. */
	protected function _hydration() {
		return null;
	}
	
	/** Override in subclasses and fill ivars from the given dictionary. */
	public function hydrateFrom($dict) {
	}
	
	public function exists() {
		$this->hydrate();
		return $this->exists;
	}
	
	
	/**
	 *  Call this to update specific object values in the db.
	 *  TODO: make this more powerful
	 */
	public function update($objects) {
		$this->preflight(false);
		
		list($table, $primary, $key) = $this->_myself();
		if (!$table || !$primary || !$key) {
			throw new Exception('Must have a table name, primary key name and the primary key itself in order to save updated values');
		}
		
		# build query
		$fields = array();
		$values = array();
		foreach ($objects as $o_key => $o_val) {
			$fields[] = "$o_key = ?";
			$values[] = $o_val;
		}
		$values[] = $key;
		
		# execute
		$qry = "UPDATE $table SET ".implode(', ', $fields)." WHERE $primary = ?";
		$stmt = $this->db->prepare($qry);
		
		return ($stmt->execute($values) > 0);
	}
	
	
	public function canDelete() {
		list($table, $primary, $key) = $this->_myself();
		return ($table && $primary && $key);
	}
	
	/**
	 *  Deletes the receiver.
	 */
	public function delete() {
		$this->preflight(false);
		
		list($table, $primary, $key) = $this->_myself();
		if (!$table || !$primary || !$key) {
			throw new Exception('Must have a table name, primary key name and the primary key itself in order to delete');
		}
		
		$stmt = $this->db->prepare("DELETE FROM $table WHERE $primary = ?");
		return $stmt->execute(array($key));
	}
}


function gen_uuid() {
	$data = openssl_random_pseudo_bytes(16);
	
	$data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0010
	$data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10
	
	return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}


?>
