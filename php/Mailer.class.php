<?php
##
##	A very basic Mailer
##
##	11. January 2010		created
##


class Mailer
{
	private $from;
	private $reply;
	private $to;
	private $subject;
	private $text;
	
	
	# Mark: Constructor
	
	public function __construct($from, $to) {
		$this->from = $from;
		$this->to = $to;
	}
	
	
	# Mark: Accessors
	
	public function from($from=null) {
		if (!is_null($from)) {
			$this->from = $from;
		}
		return $this->from;
	}
	
	public function reply($reply=null) {
		if (!is_null($reply)) {
			$this->reply = $reply;
		}
		return $this->reply;
	}
	
	public function to($to=null) {
		if (!is_null($to)) {
			$this->to = $to;
		}
		return $this->to;
	}
	
	public function subject($subject=null) {
		if (!is_null($subject)) {
			$this->subject = $subject;
		}
		return $this->subject;
	}
	
	public function body($body=null) {
		if (!is_null($body)) {
			$this->body = $body;
		}
		return $this->body;
	}
	
	protected function headers() {
		$headers = array('From: '.$this->from);
		$headers[] = 'X-Mailer: qp-Code-Mailer';
		$headers[] = 'Content-Type: text/plain; charset=utf-8';
		if ($this->reply) {
			$headers[] = 'Reply-To: '.$this->reply;
		}
		
		return implode("\r\n", $headers);
	}
	
	
	# Mark: Actions
	
	public function send() {
		return mail($this->to, $this->subject, $this->body, $this->headers());
	}
	
	public function test($truefalse) {
		echo 'mail('.$this->to.', '.$this->subject.', '.$this->body.', '.$this->headers().')';
		return $truefalse;
	}
}


?>
