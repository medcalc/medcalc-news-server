<?php

require_once(dirname(__FILE__).'/DBObject.class.php');


/**
 *  A website user.
 */
class User extends DBObject
{
	protected $name = null;
	protected $email = null;
	
	function __construct($db, $email) {
		parent::__construct($db);
		$this->email = $email;
	}
	
	public function name() {
		if (is_null($this->name)) {
			$this->hydrate();
		}
		return $this->name;
	}
	
	public function email() {
		if (is_null($this->email)) {
			$this->hydrate();
		}
		return $this->email;
	}
	
	
	/**
	 *  Retrieves the current token for the user, if there is none creates a new one and sends the login link via email.
	 */
	public function sendToken($ip) {
		$tok = Token::existingToken($this);
		if (!$tok) {
			$tok = Token::newToken($this, $ip);
			if (!$tok) {
				return false;
			}
		}
		
		$mail = new Mailer('library@ossus.ch', $this->email());
		$mail->subject('MedCalc News Token');
		$mail->body("You've got a new token that will log you in immediately!\n\n".BASE_URL."?t=".$tok->token());
		
		#return $mail->test(true);
		return $mail->send();
	}
	
	/**
	 *  Marks the user as last seen now.
	 */
	public function seen() {
		if (!$this->email) {
			throw new Exception('User does not have an email address');
		}
		
		$stmt = $this->db->prepare("UPDATE users SET lastseen = CURRENT_TIMESTAMP WHERE email = ?");
		return $stmt->execute(array($this->email));
	}
	
	protected function _hydration() {
		return array("SELECT * FROM users WHERE email = ?", $this->email);
	}
}

?>
