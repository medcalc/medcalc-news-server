<?php

require_once(dirname(__FILE__).'/DBObject.class.php');


/**
 *  Representing one news item.
 */
class NewsItem extends DBObject
{
	protected $uid;
	
	/// An HTML/JS-compatible id generated from uid
	protected $huid;
	
	protected $text;
	protected $platforms;
	protected $geo;
	protected $added;
	protected $added_by;
	protected $lastedit;
	protected $lastedit_by;
	protected $deleted;
	protected $deleted_by;
	
	function __construct($db, $uid) {
		if (!$uid) {
			throw new Exception("Cannot create a news item without uid");
		}
		parent::__construct($db);
		$this->uid = $uid;
	}
	
	public function uid() {
		return $this->uid;
	}
	
	public function huid() {
		if (is_null($this->huid)) {
			$this->huid = str_replace('-', '', $this->uid);
		}
		return $this->huid;
	}
	
	public function text() {
		return $this->text;
	}
	
	public function platforms() {
		return $this->platforms;
	}
	
	public function platformsString() {
		return ($this->platforms && count($this->platforms) > 0) ? ':'.implode(':', $this->platforms).':' : NULL;
	}
	
	public function geo() {
		return $this->geo;
	}
	
	public function added() {
		return $this->added;
	}
	
	public function addedBy() {
		return $this->added_by;
	}
	
	public function lastedit() {
		return $this->lastedit;
	}
	
	public function lasteditBy() {
		return $this->lastedit_by;
	}
	
	public function deleted() {
		return $this->deleted;
	}
	
	public function deletedBy() {
		return $this->deleted_by;
	}
	
	
	protected function _myself() {
		return array('news', 'uid', $this->uid);
	}
	
	protected function _hydration() {
		return array("SELECT * FROM news WHERE uid = ?", $this->uid);
	}
	
	public function hydrateFrom($dict) {
		if (isset($dict['text'])) {
			$this->text = $dict['text'];
		}
		if (isset($dict['platforms'])) {
			$pfs = isset($dict['platforms']) ? array() : null;
			if (!is_null($pfs)) {
				foreach (explode(':', $dict['platforms']) as $pf) {
					if (strlen($pf) > 0) {
						$pfs[] = $pf;
					}
				}
			}
			$this->platforms = $pfs;
		}
		if (isset($dict['geo'])) {
			$this->geo = $dict['geo'];
		}
		if (isset($dict['added'])) {
			$this->added = $dict['added'];
		}
		if (isset($dict['added_by'])) {
			$this->added_by = $dict['added_by'];
		}
		if (isset($dict['lastedit'])) {
			$this->lastedit = $dict['lastedit'];
		}
		if (isset($dict['lastedit_by'])) {
			$this->lastedit_by = $dict['lastedit_by'];
		}
		if (isset($dict['deleted'])) {
			$this->deleted = $dict['deleted'];
		}
		if (isset($dict['deleted_by'])) {
			$this->deleted_by = $dict['deleted_by'];
		}
	}
	
	
	public function preflight($is_insert=false) {
		if (!$this->db) {
			throw new Exception("Database is not set up");
		}
		if (!$this->uid) {
			throw new Exception("No uid given");
		}
	}
	
	public function insert(User $by) {
		$this->preflight(true);
		if (!$by) {
			throw new Exception("No user given");
		}
		
		$this->added_by = $by->email();
		$stmt = $this->db->prepare("INSERT INTO news (uid, `text`, platforms, geo, added, added_by) VALUES (?, ?, ?, ?, CURRENT_TIMESTAMP, ?)");
		return ($stmt->execute(array($this->uid, $this->text, $this->platformsString(), $this->geo, $this->added_by)) > 0);
	}
	
	/**
	 *  Update the receiver's database representation.
	 */
	public function update(User $by) {
		$this->preflight();
		if (!$by) {
			throw new Exception("No user given");
		}
		
		$this->lastedit_by = $by->email();
		$stmt = $this->db->prepare("UPDATE news SET `text` = ?, platforms = ?, lastedit = CURRENT_TIMESTAMP, lastedit_by = ? WHERE uid = ?");
		return ($stmt->execute(array($this->text, $this->platformsString(), $this->lastedit_by, $this->uid)) > 0);
	}
	
	/**
	 *  Update the receiver from values in a dictionary.
	 *  @note: Takes "platform" to set the receiver's "platforms" array to the one platform.
	 */
	public function updateFrom($dict) {
		if (isset($dict['text'])) {
			$this->text = $dict['text'];
		}
		if (isset($dict['platform'])) {
			$this->platforms = array($dict['platform']);
		}
	}
	
	/**
	 *  Deleting just creates a tombstone.
	 */
	public function delete(User $by) {
		$this->preflight();
		if (!$by) {
			throw new Exception("No user given");
		}
		if (!$this->exists() || $this->deleted) {
			throw new Exception("This news entry is already deleted");
		}
		
		$this->deleted_by = $by->email();
		$stmt = $this->db->prepare("UPDATE news SET deleted = CURRENT_TIMESTAMP, deleted_by = ? WHERE uid = ?");
		return ($stmt->execute(array($this->deleted_by, $this->uid)) > 0);
	}
	
	/**
	 *  Retrieve all news.
	 */
	public static function find($db, $platform=null) {
		if (!$db) {
			throw new Exception("Database is not set up");
		}
		
		$where = '';
		if ($platform) {
			$platform = ":$platform:";
			$where = 'WHERE platform = ? or platform is NULL';
		}
		
		$all = array();
		$stmt = $db->prepare("SELECT * FROM news $where ORDER BY added DESC");
		$stmt->execute($platform ? array($platform) : null);
		while ($row = $stmt->fetch()) {
			$news = new self($db, $row['uid']);
			$news->hydrateFrom($row);
			$news->exists = true;
			$all[] = $news;
		}
		
		return $all;
	}
}


?>
